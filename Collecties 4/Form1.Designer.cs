﻿namespace Collecties_4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbAantal = new System.Windows.Forms.TextBox();
            this.tbBenaming = new System.Windows.Forms.TextBox();
            this.tbPrijs = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnShow = new System.Windows.Forms.Button();
            this.llShow = new System.Windows.Forms.Label();
            this.llAdded = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Aantal:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Benaming:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Prijs: (0&.00)";
            // 
            // tbAantal
            // 
            this.tbAantal.Location = new System.Drawing.Point(132, 29);
            this.tbAantal.Name = "tbAantal";
            this.tbAantal.Size = new System.Drawing.Size(100, 22);
            this.tbAantal.TabIndex = 3;
            // 
            // tbBenaming
            // 
            this.tbBenaming.Location = new System.Drawing.Point(132, 64);
            this.tbBenaming.Name = "tbBenaming";
            this.tbBenaming.Size = new System.Drawing.Size(100, 22);
            this.tbBenaming.TabIndex = 4;
            // 
            // tbPrijs
            // 
            this.tbPrijs.Location = new System.Drawing.Point(132, 99);
            this.tbPrijs.Name = "tbPrijs";
            this.tbPrijs.Size = new System.Drawing.Size(100, 22);
            this.tbPrijs.TabIndex = 5;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnAdd.Location = new System.Drawing.Point(19, 147);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(315, 39);
            this.btnAdd.TabIndex = 6;
            this.btnAdd.Text = "Toevoegen aan Winkelkar";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnShow
            // 
            this.btnShow.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnShow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShow.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnShow.Location = new System.Drawing.Point(19, 203);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(315, 39);
            this.btnShow.TabIndex = 7;
            this.btnShow.Text = "Winkelkar tonen";
            this.btnShow.UseVisualStyleBackColor = false;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // llShow
            // 
            this.llShow.AutoSize = true;
            this.llShow.Location = new System.Drawing.Point(16, 264);
            this.llShow.Name = "llShow";
            this.llShow.Size = new System.Drawing.Size(0, 17);
            this.llShow.TabIndex = 8;
            // 
            // llAdded
            // 
            this.llAdded.AutoSize = true;
            this.llAdded.BackColor = System.Drawing.Color.Transparent;
            this.llAdded.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.llAdded.ForeColor = System.Drawing.Color.DarkOrange;
            this.llAdded.Location = new System.Drawing.Point(16, 118);
            this.llAdded.Name = "llAdded";
            this.llAdded.Size = new System.Drawing.Size(0, 22);
            this.llAdded.TabIndex = 9;
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnReset.Location = new System.Drawing.Point(254, 29);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(80, 70);
            this.btnReset.TabIndex = 10;
            this.btnReset.Text = "Reset Winkelkar";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 450);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.llAdded);
            this.Controls.Add(this.llShow);
            this.Controls.Add(this.btnShow);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.tbPrijs);
            this.Controls.Add(this.tbBenaming);
            this.Controls.Add(this.tbAantal);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbAantal;
        private System.Windows.Forms.TextBox tbBenaming;
        private System.Windows.Forms.TextBox tbPrijs;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.Label llShow;
        private System.Windows.Forms.Label llAdded;
        private System.Windows.Forms.Button btnReset;
    }
}

