﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Collecties_4
{
    public partial class Form1 : Form
    {
        List<WinkelkarItem> winkelkar = new List<WinkelkarItem>();
        WinkelkarItem product;
        Timer t;
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            product = new WinkelkarItem();
            int aantal, strink, everythingok = 0;
            double prijs;

            if (string.IsNullOrEmpty(tbAantal.Text) || string.IsNullOrWhiteSpace(tbAantal.Text))
            {
                MessageBox.Show("Voer een Aantal in!", "Error UI", MessageBoxButtons.OK);
            }
            else
            {
                if (Int32.TryParse(tbAantal.Text, out aantal))
                {
                    product.Aantal = aantal;
                    everythingok++;
                }
                else
                {
                    MessageBox.Show("Dit is geen correcte aantal!", "Error UI", MessageBoxButtons.OK);
                }
            }

            if (string.IsNullOrEmpty(tbBenaming.Text) || string.IsNullOrWhiteSpace(tbBenaming.Text))
            {
                MessageBox.Show("Voer een Naam in!", "Error UI", MessageBoxButtons.OK);
            }
            else
            {
                if (Int32.TryParse(tbBenaming.Text, out strink))
                {
                    MessageBox.Show("Benaming mag geen getal zijn!", "Error UI", MessageBoxButtons.OK);
                }
                else
                {
                    product.Benaming = tbBenaming.Text;
                    everythingok++;
                }
            }
            if (string.IsNullOrEmpty(tbBenaming.Text) || string.IsNullOrWhiteSpace(tbBenaming.Text))
            {
                MessageBox.Show("Voer een Prijs in!", "Error UI", MessageBoxButtons.OK);
            }
            else
            {
                if (Double.TryParse(tbPrijs.Text, out prijs))
                {
                    product.Prijs = prijs;
                    everythingok++;
                }
                else
                {
                    MessageBox.Show("Dit is geen correcte prijs!", "Error UI", MessageBoxButtons.OK);
                }
            }

            if (everythingok == 3)
            {
                winkelkar.Add(product);

                Update("Product Toegevoegd.");
            }
            else
            {

                Update("Failed to Add Product To " + Environment.NewLine + "Database self destruction " + Environment.NewLine + "in 3..2.. ");
            }



        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            ShowK();

        }
        private void ShowK()
        {
            llShow.Show();
            string s = "";
            double TotalestTotaal = 0;
            if (winkelkar.Count == 0)
            {
                s = "Er zijn nog geen producten in uw winkelkar.";

                llShow.Text = s;
                UpdateWinkelkar();
            }
            else
            {
                llShow.Text = "";
                foreach (WinkelkarItem prod in winkelkar)
                {
                    s = prod.ToString();
                    TotalestTotaal += prod.TotalePrijs;
                    llShow.Text += s;
                }
                llShow.Text += Environment.NewLine + "De Totale Winkelkar bedraagt: " + TotalestTotaal + "euro";
            }
        }

        private void Update(string eenstring)
        {
            llAdded.Show();
            llAdded.Text = eenstring;
            t = new Timer();
            t.Interval = 3000; // it will Tick in 3 seconds
            t.Tick += (sender, e) =>
            {
                llAdded.Hide();

                t.Stop();
            };
            t.Start();
        }
        private void UpdateWinkelkar()
        {
            var t = new Timer();
            t.Interval = 3000; // it will Tick in 3 seconds
            t.Tick += (s, e) =>
            {

                llShow.Hide();
                t.Stop();

            };
            t.Start();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            winkelkar.Clear();
            ShowK();
        }
    }
}
