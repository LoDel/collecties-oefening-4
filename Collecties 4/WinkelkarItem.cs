﻿using System;
using System.Windows.Forms;

namespace Collecties_4
{
    class WinkelkarItem
    {
        int _aantal;
        string _benaming;
        double _prijs;

        public WinkelkarItem()
        {
            Aantal = 1;
            Benaming = "Leeg";
            Prijs = 1;
        }
        public WinkelkarItem(int aantal, string benaming, double prijs)
        {
            Aantal = aantal;
            Benaming = benaming;
            Prijs = prijs;
        }

        public string Benaming
        {
            get { return _benaming; }
            set
            {
                if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
                {
                    MessageBox.Show("Verkeerde benaming", "Error WinkelkarItem", MessageBoxButtons.OK);
                }
                else
                {
                    _benaming = value;
                }
            }
        }
        public int Aantal
        {
            get { return _aantal; }
            set
            {
                if (value <= 0)
                {
                    MessageBox.Show("Verkeerd Aantal", "Error WinkelkarItem", MessageBoxButtons.OK);
                }
                else
                {
                    _aantal = value;
                }
            }

        }
        public double Prijs
        {
            get { return _prijs; }
            set
            {
                if (value <= 0)
                {
                    MessageBox.Show("Verkeerde prijs", "Error WinkelkarItem", MessageBoxButtons.OK);
                }
                else
                {
                    _prijs = value;
                }
            }

        }
        public double TotalePrijs
        {
            get { return Prijs * Aantal; }
        }
        public string FormattedTotalePrijs()
        {
            return TotalePrijs.ToString("0.00");
        }
        public override string ToString()
        {
            return "  " + Aantal + "     *     " + Benaming + "            :  " + FormattedTotalePrijs() + Environment.NewLine;
        }
    }
}
